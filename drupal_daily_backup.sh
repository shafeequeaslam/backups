#!/bin/sh

# Daily backup of the database, using Drush
#

# Backup directory
DIR_BACKUP=/backup

log_msg() {
  # Log to syslog
  logger -t `basename $0` "$*"

  # Echo to stdout
  LOG_TS=`date +'%H:%M:%S'`
  echo "$LOG_TS - $*"
}

DAY_OF_WEEK=drupalLiveD2S3`date '+%a'`
BACKUP_FILE=$DIR_BACKUP/backup.$DAY_OF_WEEK.tgz

log_msg "Backing up files and database to $BACKUP_FILE ..."

drush @drupal archive-dump \
  --destination=$BACKUP_FILE \
  --preserve-symlinks \
  --overwrite

RC=$?

if [ "$RC" = 0 ]; then
  log_msg "Backup completed successfully ..."
else
  log_msg "Backup exited with return code: $RC"
fi


